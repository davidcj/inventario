#!/usr/bin/python3
# -*- coding: utf-8 -*-

import contextlib
import io
import sys
import unittest
from unittest import mock
from unittest.mock import patch

from inventario import (inventario, insertar, listar, consultar,
                        agotados, pide_articulo, menu, main)


class TestInsertar(unittest.TestCase):

    def test_insertar(self):
        codigo = "ABC123"
        nombre = "Product 1"
        precio = 9.99
        cantidad = 10

        insertar(codigo, nombre, precio, cantidad)

        self.assertEqual(inventario[codigo]["nombre"], nombre)
        self.assertEqual(inventario[codigo]["precio"], precio)
        self.assertEqual(inventario[codigo]["cantidad"], cantidad)


class TestListar(unittest.TestCase):

    def setUp(self):
        inventario["ABC123"] = {"nombre": "Product 1", "precio": 9.99, "cantidad": 10}
        inventario["DEF456"] = {"nombre": "Product 2", "precio": 19.99, "cantidad": 5}

    def test_contains(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            listar()
            output = fake_out.getvalue()

        self.assertIn("ABC123", output)
        self.assertIn("Product 1", output)
        self.assertIn("DEF456", output)
        self.assertIn("Product 2", output)

    def test_exact(self):
        expected = "ABC123: Product 1, precio: 9.99, cantidad: 10\nDEF456: Product 2, precio: 19.99, cantidad: 5\n"

        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            listar()
            output = fake_out.getvalue()

        self.assertEqual(expected, output)


class TestConsultar(unittest.TestCase):

    def setUp(self):
        inventario["ABC123"] = {"nombre": "Product 1", "precio": 9.99, "cantidad": 10}

    def test_consultar(self):
        codigo = "ABC123"

        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            consultar(codigo)
            output = fake_out.getvalue()

        expected = f"{codigo}: Product 1, precio: 9.99, cantidad: 10\n"

        self.assertEqual(expected, output)


class TestAgotados(unittest.TestCase):

    def setUp(self):
        inventario["ABC123"] = {"nombre": "Product 1", "precio": 9.99, "cantidad": 0}
        inventario["DEF456"] = {"nombre": "Product 2", "precio": 19.99, "cantidad": 5}

    def test_agotados(self):
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            agotados()
            output = fake_out.getvalue()

        expected = "El articulo ABC123 se ha agotado\n"

        self.assertEqual(expected, output)


class TestPideArticulo(unittest.TestCase):

    @patch('builtins.input', side_effect=['ABC123', 'Product 1', '9.99', '10'])
    def test_pide_articulo(self, mock_inputs):
        codigo, nombre, precio, cantidad = pide_articulo()

        self.assertEqual(codigo, 'ABC123')
        self.assertEqual(nombre, 'Product 1')
        self.assertEqual(precio, 9.99)
        self.assertEqual(cantidad, 10)


class TestMenu(unittest.TestCase):

    @patch('builtins.input', side_effect=['1'])
    def test_menu_returns_option(self, mock_input):
        opcion = menu()
        self.assertEqual(opcion, '1')

    @patch('builtins.input', side_effect=['1'])
    @patch('builtins.print')
    def test_menu_prints_options(self, mock_print, mock_input):
        menu()
        calls = [
            mock.call("1. Insertar un artículo"),
            mock.call("2. Listar artículos"),
            mock.call("3. Consultar artículo"),
            mock.call("4. Artículos agotados"),
            mock.call("0. Salir")
        ]
        mock_print.assert_has_calls(calls)


expected1 = """1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: """

expected2 = """1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: """

expected3 = """1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código del artículo a consultar: ABC231: Product 2, precio: 29.99, cantidad: 5
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: """

expected4 = """1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: Código de artículo: Nombre: Precio: Cantidad: 1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: El articulo ABC123 se ha agotado
1. Insertar un artículo
2. Listar artículos
3. Consultar artículo
4. Artículos agotados
0. Salir
Opción: """


class TestMain(unittest.TestCase):

    @patch('inventario.insertar')
    def test_main_insertar(self, mock_insertar):
        sys.stdin = io.StringIO("1\nABC123\nProduct 1\n9.99\n10\n0\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        mock_insertar.assert_called_once_with('ABC123', 'Product 1', 9.99, 10)
        output = fake_out.getvalue()
        self.assertEqual(expected1, output)

    @patch('inventario.listar')
    def test_main_listar(self, mock_listar):
        sys.stdin = io.StringIO("1\nABC123\nProduct 1\n9.99\n10\n2\n0\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        mock_listar.assert_called_once()
        output = fake_out.getvalue()
        self.assertEqual(expected2, output)

    def test_main_consultar(self):
        sys.stdin = io.StringIO("1\nABC123\nProduct 1\n9.99\n10\n1\nABC231\nProduct 2\n29.99\n5\n3\nABC231\n0\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        output = fake_out.getvalue()
        self.assertEqual(expected3, output)

    def test_main_agotados(self):
        sys.stdin = io.StringIO("1\nABC123\nProduct 1\n9.99\n0\n1\nABC231\nProduct 2\n29.99\n5\n4\n0\n")
        with patch('sys.stdout', new=io.StringIO()) as fake_out:
            main()
        output = fake_out.getvalue()
        self.assertEqual(expected4, output)


if __name__ == '__main__':
    unittest.main()
